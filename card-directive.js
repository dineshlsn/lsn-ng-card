 
(function(){
	
'use strict';
	
angular.module('lsnNgModule', []);

/**
 * @ngdoc directive
 * @name core.Directives.cardDirectiv
 * @description cardDirectiv directive
 */
angular
    .module('lsnNgModule')
    .directive('cardDirective', [
        function() {
            return {
                // name: '',
                // priority: 1,
                // terminal: true,
                // scope: {}, // {} = isolate, true = child, false/undefined = no change
                // controller: function($scope, $element, $attrs, $transclude) {},
                // require: 'ngModel', // Array = multiple requires, ? = optional, ^ = check parent elements
                // restrict: 'A', // E = Element, A = Attribute, C = Class, M = Comment
                // template: '',
                // templateUrl: '',
                // replace: true,
                // transclude: true,
                // compile: function(tElement, tAttrs, function transclude(function(scope, cloneLinkingFn){ return function linking(scope, elm, attrs){}})),
				restrict: 'E',
				scope: {
					shareCallback: '&',
                    data: '=data'
                },
				templateUrl:  function(tElement, tAttrs) {
					return tAttrs.templateUrl;
				},
                link: function( $scope, iElm, iAttrs, controller) {
					
				 
					 $scope.shareMessage = function (category, contestId) {
						 
						 	var tempObj = {'data': {
												'category' : category,
												'contestId' : contestId
										   }};
						
							$scope.shareCallback(tempObj);
						
					};

                },
				controller: function($scope){
					 
				}
            };
        }
    ]);

})();